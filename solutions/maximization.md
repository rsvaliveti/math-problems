# Maximization

## Problem Statement
if $a+b+c+d= 8$ and $ab+ac+ad+bc+bd+cd=12$, what is the maximum value of $d$?

## Solution
1. From the given equations, we can deduce that $a^2+b^2+c^2+d^2 = 8^2 - 2*12 = 40$. Geometrically, the solution space is the intersection of a plane and  a sphere in 4 dimensions. This is a nice way to visualize the solution space. 
2. Hence two "trivial" upper bounds for $d$ are 8 and $\sqrt{40}$ . 
3. We use Cauchy-Schwartz inequality concerning the dot product of two vectors. This establishes that the magnitude of the dot product of two vectors is at most the product of the magnitude of the two vectors.
4. The vectors we use are $X=<a,b,c>$ and $Y=<1,1,1>$.  Using the Cauchy-Schwartz inequality for these vectors, we get $(a+b+c)^2 \leq (a^2+b^2+c^2) \cdot 3$. That is $(8-d)^2 \leq 3 (40-d^2)$ which simplifies to $d^2-4d-14 \leq 0$. This yields an upper bound of $2+3\sqrt{2}$ for $d$ -- which is one of the roots of the quadratic on the LHS
5. Now we need to prove that $d=2+3\sqrt{2}$ is actually a feasible point. 
6. With the chosen value of $d$, we get $a+b+c = 6-3\sqrt{2}$ and $a^2+b^2+c^2 = 3(2-\sqrt{2})^2$ . Now this is the problem of finding the intersection of a plane and sphere in 3D. We can establish that this plane is actually a tangent to the sphere -- because the radius of the sphere matches the perpendicular distance from (0,0,0) to the plane -- and that point on the plane has equal values for $a,b, c = 2-\sqrt{2}$. 

